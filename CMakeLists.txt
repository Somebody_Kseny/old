cmake_minimum_required(VERSION 3.15)
project(ready)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})

find_package(SFML 2 REQUIRED network audio graphics window system)
include_directories(${SFML_INCLUDE_DIR})
include_directories(headers/)

add_executable(
        ready
        main.cpp
        game.cpp
        tinyxml2.cpp
        TmxLevel.cpp
        player.cpp
        map.cpp
       # cmake_modules/candidate.h cmake_modules/something.cpp
            )
target_link_libraries(ready  ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})