//
// Created by kseny on 04.12.2019.
//

#include <iostream>
#include <SFML/Graphics/RenderWindow.hpp>
#include "headers/game.h"
//#include "headers/TmxLevel.h"
//#include "headers/map.h"
//#include "headers/player.h"

Game::Game()
        : mWindow(sf::VideoMode (1280,960), "Relax"),   // , sf::Style::Fullscreen
        mineMap("../source/yes_third.tmx"){}

void Game::run() {
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;

    while (mWindow.isOpen()) {
        processEvents();
        timeSinceLastUpdate += clock.restart();
        while (timeSinceLastUpdate > TimePerFrame) {
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();
            update(TimePerFrame);
        }
        render();
    }
}


void Game::update(sf::Time deltaTime) {
    mineMap.getGirl()->move(deltaTime, mineMap.getWalls());
    mineMap.getBoy()->move(deltaTime, mineMap.getWalls());
}

void Game::render()  {
    mWindow.clear();
    mineMap.getLevel().Draw(mWindow);
    mWindow.draw(mineMap.getPlayerObject("boy").sprite);
    mWindow.draw(mineMap.getPlayerObject("girl").sprite);
    mWindow.display();
}

void Game::processEvents()  {
    sf::Event event;
    while (mWindow.pollEvent(event))
    {
        switch (event.type)
        {

            case sf::Event::KeyPressed:
                handlePlayerInput(event.key.code, true);
                break;
            case sf::Event::KeyReleased:
                handlePlayerInput(event.key.code, false);
                break;
            case sf::Event::Closed:
                mWindow.close();
                break;
        }
    }
}


void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed) {
    if (key == sf::Keyboard::W)
        mineMap.getGirl()->setMoving(0, isPressed);
    else if (key == sf::Keyboard::A) {
        mineMap.getGirl()->setMoving(1, isPressed);
        mineMap.getGirl()->getObject().sprite.setTextureRect(sf::IntRect(0,100,100,100));
    }
    else if (key == sf::Keyboard::S)
        mineMap.getGirl()->setMoving(2, isPressed);
    else if (key == sf::Keyboard::D)
        mineMap.getGirl()->setMoving(3, isPressed);

    else if (key == sf::Keyboard::Up)
        mineMap.getBoy()->setMoving(0, isPressed);
    else if (key == sf::Keyboard::Left)
        mineMap.getBoy()->setMoving(1, isPressed);
    else if (key == sf::Keyboard::Down)
        mineMap.getBoy()->setMoving(2, isPressed);
    else if (key == sf::Keyboard::Right)
        mineMap.getBoy()->setMoving(3, isPressed);

    else if (key == sf::Keyboard::Escape)
         mWindow.close();
}

