//
// Created by kseny on 04.12.2019.
//

#include <iostream>
#include "headers/player.h"


Player::Player(TmxLevel mLevel, std::string name){
    obj = mLevel.GetFirstObject(name);
    bonuses = 0;
    //obj.sprite.setPosition(obj.rect.left, obj.rect.top);
    //std::cout << "name "<<obj.name<<" x y "<<obj.GetPropertyInt("x")<<obj.rect.top<<std::endl;
    //выбор картинки спрайта
    if (name == "boy"){
        if (!userTexture.loadFromFile("../source/boy.png"))
        {
// Handle loading error
        }
        obj.sprite.setTexture(userTexture);
        obj.sprite.setTextureRect(getRectNumber(3,4));
    } else {
        if (name == "girl"){
            if (!userTexture.loadFromFile("../source/girl.png"))
            {
// Handle loading error
            }
            obj.sprite.setTexture(userTexture);
            obj.sprite.setTextureRect(sf::IntRect(0,0,100,100));
        } else {
            //77
        }
    }
};


sf::IntRect Player::getRectNumber(int num, int all){
    sf::Vector2u size = this->userTexture.getSize();
    sf::IntRect rect;
    rect.width = size.x/all;
    rect.height = size.y;
    rect.left = (num-1)*(size.x/all);
    rect.top = 0;
    return rect;
};

TmxObject Player::getObject(){
    return this->obj;
};
std::array<bool, 4> Player::getMoving(){
    return this->moving;
};
void Player::setMoving(int n, bool meaning){
    if ((n < 0)||(n>3)) {
        std::cout << "Player::setMoving wrong n" << std::endl;
    }
    this->moving[n] = meaning;

};

void Player::move(sf::Time TimePerFrame, std::vector <TmxObject> walls){
    movement.x = 0; movement.y = 0;
    obj.sprite.setTextureRect(sf::IntRect(0,0,100,100));
    if (moving[0]==true) {
        movement.y -= speedOfHero;
        std::cout << "up" << std::endl;
    }
    if (moving[1]==true){
        movement.x -= speedOfHero;
        obj.sprite.setTextureRect(sf::IntRect(0,100,100,100));
        std::cout << "left" << std::endl;
    }
    if (moving[2]==true){
        movement.y += speedOfHero;
        std::cout << "down" << std::endl;
    }
    if (moving[3]==true){
        movement.x += speedOfHero;
        obj.sprite.setTextureRect(sf::IntRect(130,100,100,100));
        std::cout << "right" << std::endl;
    }
    //obrabotka peresechenii
    std::vector <TmxObject> :: iterator it;
    int i = 0;
    for (it = walls.begin(); it != walls.end(); it++){
        if ((obj.rect.top == walls[i].rect.top - walls[i].rect.height )||
            (obj.rect.left == walls[i].rect.left + walls[i].rect.width ) ||
            (obj.rect.top - obj.rect.height == walls[i].rect.top)||
            (obj.rect.top == walls[i].rect.top - walls[i].rect.height ))
        if(obj.rect.intersects(walls[i].rect)==true){
            std::cout <<"======intersects!!========"<<std::endl;
            movement.x = 0; movement.y = 0;
        };

        //obj.sprite.setPosition(walls[i].rect.left, walls[i].rect.top);



        i++;
        }



    obj.MoveBy(movement * TimePerFrame.asSeconds());
    std::cout<<"movement ("<<movement.x<<", "<<movement.y<<")"<<std::endl;
};
