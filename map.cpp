//
// Created by kseny on 05.12.2019.
//

#include <iostream>
#include "headers/map.h"
#include "headers/TmxLevel.h"


Map::Map (std::string way){
    try{
        mLevel.LoadFromFile(way); //HERE
    }
    catch (std::runtime_error &ex)
    {
        std::cerr << ex.what() << std::endl;
    }
    myGirl = new Player (mLevel, "girl");
    myBoy = new Player (mLevel, "boy");
    walls = mLevel.GetAllObjects("wall");
};

void Map::showStatic() {
};

TmxLevel Map::getLevel(){
    return this->mLevel;
};
TmxObject Map::getPlayerObject(std::string name){
    if (name == "boy"){
        return myBoy->getObject();
    } else {
        if (name == "girl"){
            return myGirl->getObject();
        }
    }
    std::cout << "what type of hero in get player object?.. (class map)" << std::endl;
};
//noooo
std::vector <TmxObject> Map::getObjects(std::string name){
    this->mLevel.GetAllObjects(name);
};
std::array<bool, 4> Map::getPlayerMoving(std::string name){
    if (name == "boy"){
        return myBoy->getMoving();
    } else {
        if (name == "girl"){
            return myGirl->getMoving();
        }
    }
    std::cout << "what type of hero in get player moving?.. (class map)" << std::endl;
};

void Map::setPlayerMoving(int n, bool meaning){
    if ((n < 0)||(n>3)) {
        std::cout << "Map::setPlayerMoving wrong n" << std::endl;
    }
    myGirl->setMoving(n, meaning);
};

Player* Map::getGirl(){
    return this->myGirl;
};
Player* Map::getBoy(){
    return this->myBoy;
};
std::vector <TmxObject> Map::getWalls(){
    return this->walls;
};