#include <iostream>

#include "headers/game.h"

int main() {

    //std::cout << "Hello, World!" << std::endl;

    Game game;
    game.run();

    return 0;
}
