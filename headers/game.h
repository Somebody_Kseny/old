//
// Created by kseny on 04.12.2019.
//

#ifndef READY_GAME_H
#define READY_GAME_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "TmxLevel.h"
#include "map.h"

class Game {
public:
    Game();
    void run();
private:
    void processEvents();
    void update(sf::Time);
    void render();

    //TmxLevel mgLevel;
    void handlePlayerInput(sf::Keyboard::Key, bool);
    bool mIsMovingDown = false, mIsMovingUp = false, mIsMovingLeft = false, mIsMovingRight = false;
    const sf::Time TimePerFrame = sf::seconds(1.f / 60.f);
    const int speedOfHero = 180.f;
    sf::RenderWindow mWindow;
    Map mineMap;
    //Player *somebody;
    //TmxLevel mgLevel;
protected:
};

#endif //READY_GAME_H
