//
// Created by kseny on 04.12.2019.
//

#ifndef READY_PLAYER_H
#define READY_PLAYER_H

#include "TmxLevel.h"

//enum gender {boy, girl};

class Player {
public:
    Player()= default;
    Player(TmxLevel mLevel, std::string name);
    //void setObject(TmxObject);
    void getBonuse();
    TmxObject getObject();
    std::array<bool, 4> getMoving();
    void setMoving(int n, bool meaning);
    void move(sf::Time TimePerFrame, std::vector <TmxObject> walls);
private:
    sf::Texture userTexture;
    TmxObject obj;
    std::array<bool, 4> moving = {false, false, false, false}; // 0-W 1-A 2-S 3-D
    sf::Vector2f movement = sf::Vector2f (0,0);
    const int speedOfHero = 180.f;
    int bonuses;

    sf::IntRect getRectNumber(int num, int all); //  для изображения из одинаковых прямоугольников в одну строку
protected:
};



#endif //READY_PLAYER_H
