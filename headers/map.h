//
// Created by kseny on 05.12.2019.
//

#ifndef READY_MAP_H
#define READY_MAP_H

#include "player.h"
#include "TmxLevel.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

class Map{
public:
    Map() = default;
    Map(std::string);
    void create();
    void showStatic();
    TmxLevel getLevel();
    std::vector <TmxObject> getObjects (std::string name);
    TmxObject getPlayerObject(std::string name);
    std::array<bool, 4> getPlayerMoving(std::string name);
    void setPlayerMoving(int n, bool meaning);
    Player* getGirl();
    Player* getBoy();
    std::vector <TmxObject> getWalls();
private:
    TmxLevel mLevel;
    std::vector <TmxObject> walls;
    sf::Texture userTexture;
    Player *myBoy;
    Player *myGirl;
protected:
};

#endif //READY_MAP_H
