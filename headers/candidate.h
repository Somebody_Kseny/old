//
// Created by kseny on 05.12.2019.
//

#ifndef READY_CANDIDATE_H
#define READY_CANDIDATE_H

#include <memory>

struct request {
    // object  в любом виде - сразу с id
    // при необходимости - доп. информаия
    std::string name; //имя объекта - объединенное - платформа, дверь
    std::string type; //тип, уточнение, замена номеру. или не нужно...
};

class Candidate {
public:
    virtual void handle (request);
    virtual void goNext(std::unique_ptr<Candidate>);
};

#endif //READY_CANDIDATE_H
