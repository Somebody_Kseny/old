//
// Created by kseny on 05.12.2019.
//

#ifndef READY_SOMETHING_H
#define READY_SOMETHING_H


#include "TmxLevel.h"
#include "candidate.h"

class Something: public TmxObject, public Candidate {
    void handle (request);
    void goNext(std::unique_ptr<Candidate>);
};


#endif //READY_SOMETHING_H
